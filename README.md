# Yday : Pôle infrastucture et reseau - Projet creation d'une infrastructure Active Directory

Sommaire :
* [Présentation du projet](#Présentation du projet)
* [Annexes](#Annexes)


## Présentation du projet : 

Le but de ce projet et de monter une infrastructure complète dans l'environement Active Directory de Microsoft.
Pour que cette infrastructure soit complète, elle devra comprendre : 

    
* 2 serveurs AD DS en redondance pour éviter une perte de disponibilité
* 1 serveur DHCP
* 1 serveur DNS
* 1 serveur de monitoring
* 1 serveur de fichier
* 1 ou plusieurs postes Windows administrés par GPO

## Annexes

Mot de passe recovery de l'annuaire : 
Ynov_infra


Présentation :

Clément : 
	• Présentation général du projet
		○ Logiciel
		○ Infra réseau (Diagramme et table d'adressage)
	• Partie technique
		○ Serveur DFS (Intérêt & Installation)
		○ Réplication DFS (Intérêt & Installation)
		○ GPO
Florentin:
	• Partie technique
		○ Serveur AD (Intérêt & Installation)
		○ Serveur DNS
		○ Serveur DHCP
		○ Infrastructure Hyper-V (Routeur)

Contenue de la démo :

	• Fonctionnement de la réplication AD
		○ Créer un User et observer la réplication
		○ Créer une GPO et observer la réplication
	• Fonctionnement réplication DHCP
		○ Mettre en panne un serveur DHCP
	• Fonctionnement réplication DNS
		○ Mettre en panne serveur DNS
	• Fonctionnement réplication DFS
		○ Mettre en panne serveur DFS
	• Démo Hyper-V
		○ Différente config des serveur
		○ Config du routeur
Config des PC